package com.avsgames.data;

import java.io.Serializable;

/**
 * Created by stoarch on 03.03.2016.
 */
public class PutChanel implements Serializable{
    private int id;
    private int number;
    private boolean active;

    public static final int TEMPERATURE  = 1;
    public static final int PRESSURE = 2;
    public static final int FLOWRATE = 3;

     public  PutChanel()
     {
        chanelValues = new PutChanelValues();
     }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String caption;

    private PutChanelValues chanelValues;

    public PutChanelValues getChanelValues(){ return chanelValues; }

    public void setChanelValues(PutChanelValues values){ chanelValues = values; }

    public void addValue(PutChanelValue value) {
        chanelValues.add(value);
    }

    public PutChanelValue getValue(int position) {
        return chanelValues.getValue(position);
    }

    public PutChanelValue findValue(int kind) {
        return chanelValues.find( kind );
    }
}
