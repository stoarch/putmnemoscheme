package com.avsgames.data;

/**
 * Created by kipserver on 15.04.2016.
 */
public class PutParamArchive {
    private Put put;

    public void setPut(Put put) {
        this.put = put;
    }

    public Put getPut() {
        return put;
    }
}
