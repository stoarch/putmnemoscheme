package com.avsgames.data;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kipserver on 06.04.2016.
 */
public class PutChanelValue implements Serializable {
    private int chanelId;
    private int paramKindId;
    private double value;
    private String dateQuery;
    private boolean valid;
    private Date dateQueryDate;

    public int getChanelId() {
        return chanelId;
    }
    public int getParamKindId(){ return paramKindId; }
    public double getValue(){ return value; }
    public String getDateQuery(){ return dateQuery; }
    public Date getDateQueryDate(){ return dateQueryDate; }
    public boolean getValid(){ return valid; }

    public void setChanelId(int chanelId) {
        this.chanelId = chanelId;
    }

    public void setParamKindId(int value){ paramKindId = value;}
    public void setValue(double val){ value = val; }
    public void setDateQuery(String val) throws ParseException
    {
        dateQuery = val;
        dateQueryDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(val.substring(0, val.length() - 6));
    }
    public void setValid( boolean val ){ valid = val; }
}
