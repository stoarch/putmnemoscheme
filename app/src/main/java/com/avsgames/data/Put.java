package com.avsgames.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by stoarch on 02.03.2016.
 */
public class Put implements Serializable {
    private ArrayList<PutChanel> chanels;
    private HashMap<Integer, PutChanel> chanelsMap;

    public Put() {
        chanels = new ArrayList<PutChanel>();
        chanelsMap = new HashMap<>();
    }

    public String toString() {
        return getCaption();
    }

    public void addChanel(PutChanel chanel) {
        chanels.add(chanel);
        chanelsMap.put(chanel.getId(), chanel);
    }

    public int getChanelsCount() {
        return chanels.size();
    }

    public PutChanel getChanel(int index) {
        return chanels.get(index);
    }

    public ArrayList<PutChanel> getPutChanels(){ return chanels; }

    public int getId() {
        return id;
    }

    public String getIdString() {
        return String.valueOf(id);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String[] getChanelsCaptions()
    {
        String[] res = new String[chanels.size()];
        for (int i =0; i < chanels.size(); i++)
        {
            res[i] = chanels.get(i).getCaption();
        }

        return res;
    }



    private int id;
    private String name;
    private String caption;
    private boolean active;

    public int getPosition() {
        return position;
    }

    public String getPosString()
    {
        return String.valueOf(position);
    }

    public void setPosition(int number) {
        this.position = number;
    }

    private int position; //in list from 0 to receive from REST service

    public PutChanel findChanel(int chanelId) {
        return chanelsMap.get(chanelId);
    }
}
