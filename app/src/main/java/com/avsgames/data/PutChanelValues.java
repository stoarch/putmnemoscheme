package com.avsgames.data;

import com.avsgames.data.PutChanelValue;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kipserver on 06.04.2016.
 */
public class PutChanelValues implements Serializable {
    private ArrayList<PutChanelValue> values = new ArrayList<PutChanelValue>();

    public void add(PutChanelValue value)
    {
        values.add(value);
    }

    public ArrayList<PutChanelValue> getValues(){ return values; }

    public PutChanelValue getValue(int position) {
        return values.get(position);
    }

    public int getCount() {
        return values.size();
    }

    public PutChanelValue find(int kind) {

        for (int i = 0; i < values.size(); i ++ )
        {
            if( values.get(i).getParamKindId() == kind)
                return values.get(i);
        }
        return null;
    }
}
