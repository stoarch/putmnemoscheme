package com.avsgames.providers;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.avsgames.data.Put;
import com.avsgames.data.PutChanel;
import com.avsgames.data.PutChanelValue;
import com.avsgames.data.PutChanelValues;
import com.avsgames.data.PutParamArchive;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by stoarch on 03.03.2016.
 */
public class PutReceiver {

    Context context;

    public PutReceiver( Context contextParm )
    {
        context = contextParm;
    }

    final static String ServiceURL = "http://185.48.149.157:9000";

    public ArrayList<Put> getPutList()
    {
        ArrayList<Put> putList = null;
        try {
            DownloadTextTask downloader = new DownloadTextTask(context);
            String putListJson = downloader.execute( ServiceURL + "/puts").get();
            putList = parsePutListJson(putListJson);
        } catch (Exception e) {
            Log.e("PutReceiver:>>", "Error:" + e.getClass().getName() + " Message:" + e.getMessage
                    ());
        }

        return  putList;
    }


    public Put getPut( String no )
    {
        Put put = null;
        try {
            DownloadTextTask downloader = new DownloadTextTask(context);
            String putJson = downloader.execute(ServiceURL + "/puts/" + no + "/chanels").get();
            put = parsePutJson(putJson);

            int number = Integer.parseInt(no);

            put.setPosition(number);
        } catch (Exception e) {
            Log.e("PutReceiver:>>", "Error:" + e.getClass().getName() + " Message:" + e.getMessage());
        }

        return  put;
    }

    public PutChanelValues getPutChanelValues( int putNo, int chanelNo )
    {
        PutChanelValues putValues = null;
        try {
            DownloadTextTask downloader = new DownloadTextTask(context);
            String valsJson = downloader.execute(ServiceURL + "/puts/" + putNo + "/chanels/" + chanelNo + "/values").get();
            putValues = parsePutChanelValuesJson(valsJson);
        } catch (Exception e) {
            Log.e("PutReceiver:>>", "Error:" + e.getClass().getName() + " Message:" + e.getMessage());
        }

        return  putValues;
    }

    private PutChanelValues parsePutChanelValuesJson( String json )
    {
        PutChanelValues putVals =  null;
        try {
            JSONObject item = new JSONObject(json);
            JSONObject chanel = item.getJSONObject("chanel");
            JSONObject temperature = item.getJSONObject("temperature");
            JSONObject pressure = item.getJSONObject("pressure");
            JSONObject flowrate = item.getJSONObject("flowrate");

            putVals = new PutChanelValues();

            PutChanelValue value = new PutChanelValue();
            value.setParamKindId(temperature.getInt("param_kind_id"));
            value.setValue(temperature.getDouble("value"));
            value.setDateQuery(temperature.getString("date_query"));
            value.setValid(temperature.getBoolean("valid"));

            putVals.add( value );

            value = new PutChanelValue();
            value.setParamKindId(pressure.getInt("param_kind_id"));
            value.setValue(pressure.getDouble("value"));
            value.setDateQuery(pressure.getString("date_query"));
            value.setValid(pressure.getBoolean("valid"));

            putVals.add( value );

            value = new PutChanelValue();
            value.setParamKindId(flowrate.getInt("param_kind_id"));
            value.setValue(flowrate.getDouble("value"));
            value.setDateQuery(flowrate.getString("date_query"));
            value.setValid(flowrate.getBoolean("valid"));

            putVals.add( value );

        }
        catch( Exception e )
        {
            Log.e( "PutReceiver:>>", e.getMessage() );
        }

        return putVals;
    }

    public Put getPutValues( int no )
    {
        Put put = null;
        try {
            DownloadTextTask downloader = new DownloadTextTask(context);
            String putJson = downloader.execute(ServiceURL + "/puts/" + Integer.toString( no ) + "/values").get();
            put = parsePutValuesJson(putJson);
            put.setPosition(no);
        } catch (Exception e) {
            Log.e("PutReceiver:>>", "Error:" + e.getClass().getName() + " Message:" + e.getMessage());
        }

        return  put;
    }

    private Put parsePutValuesJson( String json )
    {
        Put put =  null;
        try {
            JSONObject item = new JSONObject(json);

            put = new Put();
            put.setId(item.getInt("put_id"));
            put.setName(item.getString("put_name"));
            put.setCaption(item.getString("caption"));
            put.setPosition( UNKNOWN_POSITION );
            //{"put":{
            //      "caption":"PK-3",
            //      "active":true,
            //      "id":9,
            //      "name":"PUT11",
            //      "parallel":true,
            //      "vzljot_device_code":"21",
            //      "order":3,
            //      "chanel_count":6
            // },
            // "values":{
            //      "152":[
            //          {"chanel_id":152,"param_kind_id":3,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794327,"valid":true},
            //          {"chanel_id":152,"param_kind_id":2,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794326,"valid":true},
            //          {"chanel_id":152,"param_kind_id":1,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794325,"valid":true}],
            //      "151":[
            //          {"chanel_id":151,"param_kind_id":2,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794324,"valid":true},
            //          {"chanel_id":151,"param_kind_id":1,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794323,"valid":true},
            //          {"chanel_id":151,"param_kind_id":3,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794322,"valid":true}],
            //      "150":[
            //          {"chanel_id":150,"param_kind_id":3,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794321,"valid":true},
            //          {"chanel_id":150,"param_kind_id":2,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794320,"valid":true},
            //          {"chanel_id":150,"param_kind_id":1,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794319,"valid":true}],
            //      "149":[
            //          {"chanel_id":149,"param_kind_id":2,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794318,"valid":true},
            //          {"chanel_id":149,"param_kind_id":3,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794317,"valid":true},
            //          {"chanel_id":149,"param_kind_id":1,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794316,"valid":true}],
            //      "23":[
            //          {"chanel_id":23,"param_kind_id":3,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794315,"valid":true},
            //          {"chanel_id":23,"param_kind_id":2,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794314,"valid":true},
            //          {"chanel_id":23,"param_kind_id":1,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794313,"valid":true}],
            //      "22":[
            //          {"chanel_id":22,"param_kind_id":3,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794312,"valid":true},
            //          {"chanel_id":22,"param_kind_id":2,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794311,"valid":true},
            //          {"chanel_id":22,"param_kind_id":1,"value":0.0,"date_query":"2016-04-04 21:32:59 +0600","id":18794310,"valid":true}]}}

            JSONObject values = item.getJSONObject("values");
            JSONArray names = values.names();
            for (int i = 0; i < names.length(); i++ )
            {
                //todo: implement this
            }
        }
        catch( Exception e )
        {
            Log.e( "PutReceiver:>>", e.getMessage() );
        }

        return put;
    }

    public final  int UNKNOWN_POSITION = -1;

    private Put parsePutJson( String json )
    {
        Put put =  null;
        try {
            JSONObject item = new JSONObject(json);

            put = parsePut(item);

            parsePutChanels(put, item);


        }
        catch( Exception e )
        {
            Log.e( "PutReceiver:>>", e.getMessage() );
        }

        return put;
    }

    private void parseArcPutChanels(Put put, JSONObject item) throws JSONException {
        JSONArray chanels = item.getJSONArray("chanels");
        for (int i = 0; i < chanels.length(); i++){
            JSONObject jsonChanel = chanels.getJSONObject(i);
            PutChanel chanel = parseArcPutChanel(jsonChanel);
            chanel.setNumber(i);

            put.addChanel( chanel );
        }
    }

    private void parsePutChanels(Put put, JSONObject item) throws JSONException {
        JSONArray chanels = item.getJSONArray("chanels");
        for (int i = 0; i < chanels.length(); i++){
            JSONObject jsonChanel = chanels.getJSONObject(i);
            PutChanel chanel = parsePutChanel(jsonChanel);
            chanel.setNumber(i);

            put.addChanel( chanel );
        }
    }

    @NonNull
    private PutChanel parseArcPutChanel(JSONObject jsonChanel) throws JSONException {
        PutChanel chanel = new PutChanel();
        chanel.setId( jsonChanel.getInt("id") );
        chanel.setCaption(jsonChanel.getString("caption"));
        chanel.setActive(jsonChanel.getBoolean("active"));
        return chanel;
    }

    @NonNull
    private PutChanel parsePutChanel(JSONObject jsonChanel) throws JSONException {
        PutChanel chanel = new PutChanel();
        chanel.setId( jsonChanel.getInt("chanel_id") );
        chanel.setCaption(jsonChanel.getString("caption"));
        chanel.setActive(jsonChanel.getBoolean("active"));
        return chanel;
    }

    // private //
    private ArrayList<Put> parsePutListJson( String json )
    {
        ArrayList<Put> res = new ArrayList<Put>();
        JSONArray dataJO = null;
        int id = 0;
        String name = "";
        String caption = "";
        boolean active = false;

        try {
            dataJO = new JSONArray(json);
            for (int i = 0; i < dataJO.length(); i++) {
                JSONObject item = dataJO.getJSONObject(i);

                Put put = new Put();
                put.setId(item.getInt("put_id"));
                put.setName(item.getString("name"));
                put.setCaption(item.getString("caption"));
                put.setActive(item.getBoolean("active"));
                put.setPosition(i);

                res.add(put);
            }
        }
        catch( Exception e )
        {
            Log.e( "PutReceiver:>>", e.getMessage() );
        }

        return res;
    }

    String urlText = "";

    public PutParamArchive getArchive(int putNo, String archiveKind, String dateStart, String dateEnd) {
        PutParamArchive paramArchive = null;
        try {
            DownloadTextTask downloader = new DownloadTextTask(context);
            String valsJson = downloader.execute(ServiceURL + "/put/" + putNo + "/archive/" + archiveKind + "/\"" + dateStart + "\"/\"" + dateEnd + "\"").get();
            paramArchive = parseParamArchiveJSON(valsJson);
        } catch (Exception e) {
            Log.e("PutReceiver:>>", "Error:" + e.getClass().getName() + " Message:" + e.getMessage());
        }

        return  paramArchive;
    }

    private PutParamArchive parseParamArchiveJSON(String valsJson) {
        PutParamArchive archive = new PutParamArchive();

        try {
            JSONObject json = new JSONObject(valsJson);
            Put put = parseArcPut(json.getJSONObject("put"));
            archive.setPut(put);

            parseArcPutChanels(put, json);

            JSONArray values = json.getJSONArray("values");
            for (int i = 0; i < values.length(); i++) {
                JSONObject item = values.getJSONObject(i);
                PutChanelValue value = parsePutValueJson(item);

                PutChanel chanel = put.findChanel( value.getChanelId() );
                chanel.addValue( value );
            }
        }
        catch( Exception e )
        {
            Log.e( "PutReceiver:>>", e.getMessage() );
        }

        return archive;
    }

    private PutChanelValue parsePutValueJson(JSONObject item)
            throws  JSONException, ParseException
    {
        PutChanelValue value = new PutChanelValue();
        value.setChanelId(item.getInt("chanel_id"));
        value.setParamKindId(item.getInt("param_kind_id"));
        value.setValue(item.getDouble("value"));
        value.setDateQuery(item.getString("date_query"));
        value.setValid(item.getBoolean("valid"));
        return value;
    }

    private Put parseArcPut(JSONObject item) throws JSONException {
        Put put = new Put();
        put.setId(item.getInt("id"));
        put.setName(item.getString("name"));
        put.setCaption(item.getString("caption"));
        put.setActive(item.getBoolean("active"));
        put.setPosition(UNKNOWN_POSITION);
        return put;
    }
    @NonNull
    private Put parsePut(JSONObject item) throws JSONException {
        Put put = new Put();
        put.setId(item.getInt("put_id"));
        put.setName(item.getString("put_name"));
        put.setCaption(item.getString("caption"));
        put.setPosition(UNKNOWN_POSITION);
        return put;
    }

    private class DownloadTextTask extends AsyncTask<String, Void, String> {
        private final Context context;
        public DownloadTextTask(Context contextParm)
        {
            context = contextParm;
        }
        @Override
        protected String doInBackground(String... urls) {
            try {
                return getToServer(urls[0]);
            }catch (Exception e){
                Log.e("PutReceiver:>>", e.getLocalizedMessage());
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result){
            urlText = result;
        }

    }

    private String getToServer( String service ) throws Exception {
        URL url = new URL(service);
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        String res = "";
        try{
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while((line = reader.readLine()) != null)
            {
                buffer.append(line);
            }
            res = buffer.toString();
        }
        finally {

            urlConnection.disconnect();
        }
        return res;
    }

}
