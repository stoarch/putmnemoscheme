package com.avsgames.putmnemoscheme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.avsgames.data.Put;
import com.avsgames.data.PutChanel;
import com.avsgames.data.PutChanelValues;
import com.avsgames.data.PutParamArchive;
import com.avsgames.providers.PutReceiver;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ParamArchiveActivity extends AppCompatActivity {

   public static final int TEMPERATURE_ARCHIVE = 1;
   public static final int PRESSURE_ARCHIVE = 2;
   public static final int FLOWRATE_ARCHIVE = 3;

    private int activeArchive = 0;
    private int yearStart, monthStart, dayStart;
    private int yearEnd, monthEnd, dayEnd;
    private int putNo;
    private RecyclerView archiveListView;
    private PutParamArchive vals;
    private LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_param_archive);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        chart = (LineChart)findViewById(R.id.param_chart);
        chart.setPinchZoom(true);

        Intent i = getIntent();
        activeArchive = i.getIntExtra("param_id", 0);
        yearStart = i.getIntExtra("year_start", 0);
        monthStart = i.getIntExtra("month_start", 0);
        dayStart = i.getIntExtra("day_start", 0);
        yearEnd = i.getIntExtra("year_end", 0);
        monthEnd = i.getIntExtra("month_end", 0);
        dayEnd = i.getIntExtra("day_end", 0);
        putNo = i.getIntExtra("put_no", -1);

        TextView fromDate = (TextView)findViewById(R.id.param_date_start);
        TextView toDate = (TextView)findViewById(R.id.param_date_end);

        fromDate.setText(String.format("%d.%d.%d", dayStart, monthStart, yearStart));
        toDate.setText(String.format("%d.%d.%d", dayEnd, monthEnd, yearEnd));


        archiveListView = (RecyclerView)findViewById(R.id.param_list_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        archiveListView.setHasFixedSize(true);
        archiveListView.setLayoutManager(llm);

        vals = new PutParamArchive();
        PutParamArchiveAdapter adapter = new PutParamArchiveAdapter(ParamArchiveActivity.this, vals);
        archiveListView.setAdapter(adapter);

        setTitle("Архив (" + getParamCaption() + ") для " + i.getStringExtra("put_caption"));

        TextView paramCaption = (TextView)findViewById(R.id.param_caption);
        paramCaption.setText(getParamCaption());

        LoadArchive();
    }

    private void LoadArchive() {
        final PutReceiver receiver = new PutReceiver(this);
        final ProgressDialog progress = ProgressDialog.show(this, "Загрузка", "Ожидаю данные архива...", true);

        new Thread() {
            public void run() {
                String dateStart = String.format("%d.%d.%d", dayStart, monthStart, yearStart );
                String dateEnd = String.format("%d.%d.%d", dayEnd, monthEnd, yearEnd);

                vals = receiver.getArchive(putNo, getArchiveKind(activeArchive), dateStart, dateEnd);
                progress.dismiss();

                if( vals != null )
                    runOnUiThread(new Runnable() {
                                      @Override
                                      public void run() {
                            PutParamArchiveAdapter adapter = new PutParamArchiveAdapter(ParamArchiveActivity.this, vals);
                            archiveListView.setAdapter(adapter);
                            UpdateChart();
                    }
                    });
            }
        }.start();
    }

    private void UpdateChart() {
        Put put = vals.getPut();
        if( put == null )
            return;

        if( put.getChanelsCount() == 0 )
            return;

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        ArrayList<String> xVals = new ArrayList<String>();

        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        int colors[] = {R.color.param1, R.color.param2, R.color.param3, R.color.param4, R.color.param5, R.color.param6};

        for (int i = 0; i < put.getChanelsCount(); i++) {
            ArrayList<Entry> valsParm = new ArrayList<>();
            PutChanel chanel = put.getChanel(i);
            PutChanelValues chanelVals = chanel.getChanelValues();
            for (int j = 0; j < chanelVals.getCount(); j++)
            {
               Entry valEntry = new Entry((float)chanelVals.getValue(j).getValue(),j);
               valsParm.add(valEntry);

               if( i == 0 )
               {
                   xVals.add(df.format(chanelVals.getValue(j).getDateQueryDate()));
               }
            }
            LineDataSet setParm = new LineDataSet(valsParm, chanel.getCaption());
            setParm.setColor(colors[i%6]);
            setParm.setDrawCircles(false);
            dataSets.add(setParm);
        }

        LineData data = new LineData(xVals, dataSets);
        chart.setData(data);
        chart.invalidate();
    }

    private String getArchiveKind(int activeArchive) {
        switch( activeArchive )
        {
            case TEMPERATURE_ARCHIVE: return "temperature";
            case PRESSURE_ARCHIVE: return "pressure";
            case FLOWRATE_ARCHIVE: return "flowrate";
        }
        return "unknown";
    }

    private String getParamCaption() {
        switch( activeArchive )
        {
            case TEMPERATURE_ARCHIVE: return "температура";
            case PRESSURE_ARCHIVE: return "давление";
            case FLOWRATE_ARCHIVE: return "расход";
        }
        return "нет";
    }

}
