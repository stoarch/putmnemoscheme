package com.avsgames.putmnemoscheme;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.avsgames.data.PutChanelValue;

/**
 * Created by kipserver on 28.04.2016.
 */
public class ManometerView3 extends FrameLayout {
    private ImageView arrow;

    public ManometerView3(Context context){
        super(context);
        initializeViews(context);
    }

    public ManometerView3(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initializeViews(context);
    }

    public ManometerView3(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.manometer_compound_view, this);
    }

    @Override
    protected  void onFinishInflate(){
        super.onFinishInflate();

        arrow = (ImageView)this.findViewById(R.id.manometer_arrow);
    }

    public void rotate( float angle )
    {
        ObjectAnimator arrowAnimator = ObjectAnimator.ofFloat(
                arrow,
                "rotation",
                arrow.getRotation(),
                angle
        );
        arrowAnimator.setDuration(500);//ms
        arrowAnimator.start();
    }

    static final int MIN_ANGLE = -40;
    static final int MAX_ANGLE = 220;
    static final int DELTA_ANGLE = MAX_ANGLE - MIN_ANGLE;

    static final double MIN_PRESSURE = 0f;
    static  final double MAX_PRESSURE = 10f;
    static final double DELTA_PRESSURE = MAX_PRESSURE - MIN_PRESSURE;

    public void setPressure(double value) {
        float angle = MIN_ANGLE;
        if( value > 0 )
         angle = (float)(MIN_ANGLE + DELTA_ANGLE/DELTA_PRESSURE*(value));

        rotate(angle);
    }
}
