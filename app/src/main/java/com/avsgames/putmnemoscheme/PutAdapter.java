package com.avsgames.putmnemoscheme;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.avsgames.data.Put;

import java.util.ArrayList;

/**
 * Created by stoarch on 03.03.2016.
 */
public class PutAdapter extends ArrayAdapter<Put> {
    private Activity activity;
    private ArrayList<Put> puts;
    private static LayoutInflater inflater = null;

    public PutAdapter(Activity activityParm, int textViewResourceId, ArrayList<Put> putsParam) {
        super(activityParm, textViewResourceId, putsParam);
        try {
            activity = activityParm;
            puts = putsParam;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {
            Log.e("PutAdapter", e.getLocalizedMessage());
        }

    }

    private static class ViewHolder{
        TextView putName;
        TextView putCaption;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Put put = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_put, parent, false);
            // Lookup view for data population
            viewHolder.putName = (TextView) convertView.findViewById(R.id.put_item_caption);
            // Populate the data into the template view using the data object
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        // Return the completed view to render on screen
        viewHolder.putName.setText(put.getName());
        return convertView;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }


}
