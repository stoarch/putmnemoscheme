package com.avsgames.putmnemoscheme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.avsgames.data.Put;
import com.avsgames.providers.PutReceiver;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class PutListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ArrayList<Put> putList = null;
    PutReceiver receiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_put_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if( receiver == null ) {
            receiver = new PutReceiver(this.getBaseContext());
        }

        ListView putListView = (ListView)findViewById(R.id.put_list);
        if(putListView != null)
        {
            if( putList == null ){
                ProgressDialog progress = ProgressDialog.show(this, "Loading", "Wait for PUTs...", true);
               putList = receiver.getPutList();
                progress.dismiss();
            }

            ArrayAdapter<Put> putAdapter = new ArrayAdapter<Put>(this, R.layout.put_list_item_detail, R.id.put_item_caption, putList);

            putListView.setAdapter(putAdapter);

            putListView.setTextFilterEnabled(true);
            putListView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener(){
                        @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3){
                            int index = arg2;
                            Toast.makeText(getBaseContext(),
                                    "Selected " + putList.get(index).getCaption(),
                                    Toast.LENGTH_SHORT).show();

                            ProgressDialog progress = ProgressDialog.show(PutListActivity.this, "Loading", "Wait for PUTs...", true);

                            Put put = receiver.getPut(Integer.toString(index));

                            Intent i = new Intent(PutListActivity.this, PutChanelActivity.class);
                            i.putExtra("PUT_ID", putList.get(index).getId());
                            i.putExtra("PUT", put);
                            i.putExtra("PUT_NO", index);

                            progress.dismiss();
                            startActivity(i);
                        }
                    }
            );
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.put_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Toast.makeText(this, "ID:" + Integer.toString(id), Toast.LENGTH_SHORT).show();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Toast.makeText(this, "NavID:" + Integer.toString(id), Toast.LENGTH_SHORT).show();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
