package com.avsgames.putmnemoscheme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.avsgames.data.Put;
import com.avsgames.data.PutChanel;
import com.avsgames.data.PutChanelValue;
import com.avsgames.data.PutChanelValues;
import com.avsgames.providers.PutReceiver;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.Calendar;

public class PutChanelActivity extends AppCompatActivity
        implements
        com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener {

    Handler h;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private Put put;
    private int putNo;
    private int putId;
    private ArrayList<PutChanel> chanels;
    private ProgressDialog progress;
    private PutReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_put_chanel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ListView chanelListView = (ListView) findViewById(R.id.chanel_list);
        if (chanelListView != null) {
            chanelListView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener(){
                        @Override
                        public void onItemClick(AdapterView<?> adapter, View view, int index, long arg3) {

                        }
                    }
            );

            putId = getIntent().getIntExtra("PUT_ID", -1);
            putNo = getIntent().getIntExtra("PUT_NO", -1);

            put = (Put) getIntent().getSerializableExtra("PUT");

            if (put != null) {
                setTitle(put.getCaption());
                receiver = new PutReceiver(this);
                progress = ProgressDialog.show(this, "Загрузка", "Ожидаю каналы ПУТа...", true);

                new Thread() {
                    public void run() {
                        String[] chanelCaptions = put.getChanelsCaptions();
                        chanels = put.getPutChanels();
                        for (int i = 0; i < chanels.size(); i++) {
                            PutChanelValues vals = receiver.getPutChanelValues(putNo, i);
                            PutChanel chanel = chanels.get(i);
                            chanel.setChanelValues(vals);
                        }
                        progress.dismiss();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                PutChanelListAdapter chanelAdapter = new PutChanelListAdapter(PutChanelActivity.this, chanels);
                                chanelListView.setAdapter(chanelAdapter);
                            }
                        });
                    }
                }.start();

            }
        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.put_chanel_list, menu);
        return true;
    }

    private static final int TEMPERATURE_ARCHIVE = 1;
    private static final int PRESSURE_ARCHIVE = 2;
    private static final int FLOWRATE_ARCHIVE = 3;

    private int activeArchive = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Toast.makeText(this, "ID:" + Integer.toString(id), Toast.LENGTH_SHORT).show();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else {
            if (id == R.id.temperature_archive) {
                activeArchive = TEMPERATURE_ARCHIVE;
            } else if (id == R.id.pressure_archive) {
                activeArchive = PRESSURE_ARCHIVE;
            } else if (id == R.id.flowrate_archive) {
                activeArchive = FLOWRATE_ARCHIVE;
            }

            Calendar now = Calendar.getInstance();
            com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }

        return super.onOptionsItemSelected(item);
    }

    private int yearStart, monthStart, dayStart;
    private int yearEnd, monthEnd, dayEnd;

    @Override
    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        Toast.makeText(PutChanelActivity.this, "Date set", Toast.LENGTH_SHORT).show();
        yearStart = year;
        monthStart = monthOfYear;
        dayStart = dayOfMonth;

        this.yearEnd = yearEnd;
        monthEnd = monthOfYearEnd;
        dayEnd = dayOfMonthEnd;

        LoadArchive();
    }

    private void LoadArchive() {
        Intent archiveIntent = new Intent(this, ParamArchiveActivity.class);
        archiveIntent.putExtra("param_id", activeArchive);
        archiveIntent.putExtra("year_start", yearStart);
        archiveIntent.putExtra("year_end", yearEnd);
        archiveIntent.putExtra("month_start", monthStart);
        archiveIntent.putExtra("month_end", monthEnd);
        archiveIntent.putExtra("day_start", dayStart);
        archiveIntent.putExtra("day_end", dayEnd);
        archiveIntent.putExtra("put_caption", put.getCaption() );
        archiveIntent.putExtra("put_no", putNo);

        startActivity(archiveIntent);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "PutChanel Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.avsgames.putmnemoscheme/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "PutChanel Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.avsgames.putmnemoscheme/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
