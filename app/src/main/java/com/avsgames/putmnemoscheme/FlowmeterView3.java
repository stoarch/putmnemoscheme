package com.avsgames.putmnemoscheme;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by kipserver on 29.04.2016.
 */
public class FlowmeterView3 extends FrameLayout {
    private ImageView arrow;

    public FlowmeterView3(Context context){
        super(context);
        initializeViews(context);
    }

    public FlowmeterView3(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initializeViews(context);
    }

    public FlowmeterView3(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.flowmeter_view3, this);
    }

    @Override
    protected  void onFinishInflate(){
        super.onFinishInflate();

        arrow = (ImageView)this.findViewById(R.id.flowmeter_arrow);
    }

    public void translate( float dx )
    {
        ObjectAnimator arrowAnimator = ObjectAnimator.ofFloat(
                arrow,
                "translationX",
                arrow.getTranslationX(),
                dx
        );
        arrowAnimator.setDuration(500);//ms
        arrowAnimator.start();
    }

    static final float MIN_DX = 6;
    static final float MAX_DX = 171;
    static final float DELTA_DX = MAX_DX - MIN_DX;

    static final float MIN_FLOWRATE = 0f;
    static  final float MAX_FLOWRATE  = 1000f;
    static final float DELTA_FLOWRATE = MAX_FLOWRATE - MIN_FLOWRATE;

    public void setFlowrate(double value) {
        float flow = MIN_FLOWRATE;
        if( value > 0 )
            flow = (float)(MIN_DX + DELTA_DX/DELTA_FLOWRATE*(value));

        translate(flow);
    }
}
