package com.avsgames.putmnemoscheme;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.avsgames.data.PutChanel;
import com.avsgames.data.PutChanelValue;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by kipserver on 06.04.2016.
 */
public class PutChanelListAdapter extends ArrayAdapter<PutChanel> {

    private final Activity context;
    private final ArrayList<PutChanel> chanels;

    public PutChanelListAdapter(Activity acontext, ArrayList<PutChanel> achanels )
    {
        super(acontext, R.layout.lvchanel_row_layout, achanels);
        context = acontext;
        chanels = achanels;
    }

    static final int TEMPERATURE_ID = 1;
    static final int PRESSURE_ID = 2;
    static final int FLOWRATE_ID = 3;

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.lvchanel_row_layout, null, true);

        PutChanel chanel = chanels.get(position);

        TextView txtCaption = (TextView)rowView.findViewById(R.id.chanel_caption);
        txtCaption.setText( chanel.getCaption() );

        TextView txtDate = (TextView)rowView.findViewById(R.id.chanel_date);

        TextView txtTemperature = (TextView)rowView.findViewById(R.id.chanel_temperature_value);
        TextView txtPressure = (TextView)rowView.findViewById(R.id.chanel_pressure_value);
        TextView txtFlowrate = (TextView)rowView.findViewById(R.id.chanel_flowrate_value);

        DecimalFormat precision = new DecimalFormat("0.00");

        ArrayList<PutChanelValue> vals = chanel.getChanelValues().getValues();
        if( vals.size() > 0 )
            txtDate.setText( vals.get(0).getDateQuery() );


        ManometerView3 manometerView3  = (ManometerView3)rowView.findViewById(R.id.pressure_manometer);
        assert manometerView3 != null;

        ThermometerView3 thermometerView3 = (ThermometerView3)rowView.findViewById(R.id.thermometer_view);
        assert thermometerView3 != null;

        FlowmeterView3 flowmeterView3 = (FlowmeterView3)rowView.findViewById(R.id.flowmeter_view);
        assert flowmeterView3 != null;

        for (int i = 0; i < vals.size(); i++ )
        {
            PutChanelValue value = vals.get(i);
            String valStr = precision.format(value.getValue());

            int paramKind = value.getParamKindId();

            if( paramKind == TEMPERATURE_ID )
            {
                txtTemperature.setText(valStr + " C");
                thermometerView3.setTemperature(value.getValue());
            }
            else if( paramKind == PRESSURE_ID )
            {
                txtPressure.setText(valStr + " Атм");
                manometerView3.setPressure(value.getValue());
            }
            else if(paramKind == FLOWRATE_ID )
            {
                txtFlowrate.setText(valStr + " м3/ч");
                flowmeterView3.setFlowrate(value.getValue());
            }
        }

        return rowView;
    }
}
