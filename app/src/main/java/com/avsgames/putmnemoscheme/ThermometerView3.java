package com.avsgames.putmnemoscheme;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by kipserver on 29.04.2016.
 */
public class ThermometerView3 extends FrameLayout {
    private ImageView arrow;

    public ThermometerView3(Context context){
        super(context);
        initializeViews(context);
    }

    public ThermometerView3(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initializeViews(context);
    }

    public ThermometerView3(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.thermometer_view, this);
    }

    @Override
    protected  void onFinishInflate(){
        super.onFinishInflate();

        arrow = (ImageView)this.findViewById(R.id.thermometer_arrow);
    }

    public void translate( float dy )
    {
        ObjectAnimator arrowAnimator = ObjectAnimator.ofFloat(
                arrow,
                "translationY",
                arrow.getTranslationY(),
                dy
        );
        arrowAnimator.setDuration(500);//ms
        arrowAnimator.start();
    }

    static final float MIN_DY = 83;
    static final float MAX_DY = 0.5f;
    static final float DELTA_DY = MAX_DY - MIN_DY;

    static final float MIN_TEMPERATURE = 0f;
    static  final float MAX_TEMPERATURE = 100f;
    static final float DELTA_TEMPERATURE = MAX_TEMPERATURE - MIN_TEMPERATURE;

    public void setTemperature(double value) {
        float temp = MIN_TEMPERATURE;
        if( value > 0 )
            temp = (float)(MIN_DY + DELTA_DY/DELTA_TEMPERATURE*(value));

        translate(temp);
    }
}
