package com.avsgames.putmnemoscheme;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avsgames.data.Put;
import com.avsgames.data.PutChanel;
import com.avsgames.data.PutChanelValue;
import com.avsgames.data.PutParamArchive;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Created by kipserver on 15.04.2016.
 */
public class PutParamArchiveAdapter extends RecyclerView.Adapter<PutParamArchiveAdapter.ParamArchiveViewHolder>{

    public static class ParamArchiveViewHolder extends RecyclerView.ViewHolder
    {
        TextView id;
        TextView paramDate;
        TextView paramTime;

        TextView param1;
        TextView param2;
        TextView param3;
        TextView param4;
        TextView param5;
        TextView param6;

        TextView param1Date;
        TextView param2Date;
        TextView param3Date;
        TextView param4Date;
        TextView param5Date;
        TextView param6Date;

        ParamArchiveViewHolder(View view)
        {
            super(view);

            id = (TextView) view.findViewById(R.id.id);
            paramDate =  (TextView)view.findViewById(R.id.param_date);
            paramTime = (TextView)view.findViewById(R.id.param_time);

            param1 = (TextView)view.findViewById(R.id.param1);
            param2 = (TextView)view.findViewById(R.id.param2);
            param3 = (TextView)view.findViewById(R.id.param3);
            param4 = (TextView)view.findViewById(R.id.param4);
            param5 = (TextView)view.findViewById(R.id.param5);
            param6 = (TextView)view.findViewById(R.id.param6);

            param1Date = (TextView)view.findViewById(R.id.param1_date);
            param2Date = (TextView)view.findViewById(R.id.param2_date);
            param3Date = (TextView)view.findViewById(R.id.param3_date);
            param4Date = (TextView)view.findViewById(R.id.param4_date);
            param5Date = (TextView)view.findViewById(R.id.param5_date);
            param6Date = (TextView)view.findViewById(R.id.param6_date);

        }

    }
    private Activity context;
    private PutParamArchive archive;

    public PutParamArchiveAdapter(Activity context, PutParamArchive archive)
    {
        this.context = context;
        this.archive = archive;
    }

    public int getItemCount()
    {
        if( archive.getPut() != null )
            return archive.getPut().getChanel(0).getChanelValues().getValues().size();
        else
            return 0;
    }

    @Override
    public ParamArchiveViewHolder onCreateViewHolder(ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(
                        R.layout.fragment_param_archive_list_item,
                        viewGroup,
                        false);
        ParamArchiveViewHolder pvh = new ParamArchiveViewHolder(view);
        return pvh;

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView view)
    {
        super.onAttachedToRecyclerView(view);
    }

    @Override
    public void onBindViewHolder(ParamArchiveViewHolder holder, int position)
    {
        holder.id.setText(String.valueOf(position));
        DecimalFormat df = new DecimalFormat("##.00");


        Put put = archive.getPut();
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH.mm");

        holder.paramDate.setText(dayFormat.format(put.getChanel(0).getValue(0).getDateQueryDate()));

        for (int i = 0; i < put.getChanelsCount(); i++ )
        {

            PutChanel chanel = put.getChanel(i);
            PutChanelValue value = chanel.getValue(position);

            switch(i)
            {
                case 0: {
                    holder.param1.setText(df.format(value.getValue()));
                    holder.param1Date.setText(hourFormat.format(value.getDateQueryDate()));
                    holder.paramTime.setText(hourFormat.format(value.getDateQueryDate()));
                    break;
                }
                case 1: {
                    holder.param2.setText(df.format(value.getValue()));
                    holder.param2Date.setText(hourFormat.format(value.getDateQueryDate()));
                    break;
                }
                case 2: {
                    holder.param3.setText(df.format(value.getValue()));
                    holder.param3Date.setText(hourFormat.format(value.getDateQueryDate()));
                    break;
                }
                case 3: {
                    holder.param4.setText(df.format(value.getValue()));
                    holder.param4Date.setText(hourFormat.format(value.getDateQueryDate()));
                    break;
                }
                case 4: {
                    holder.param5.setText(df.format(value.getValue()));
                    holder.param5Date.setText(hourFormat.format(value.getDateQueryDate()));
                    break;
                }
                case 5: {
                    holder.param6.setText(df.format(value.getValue()));
                    holder.param6Date.setText(hourFormat.format(value.getDateQueryDate()));
                    break;
                }
            }

            if( i > 5 )
                break;
        }
    }


}
